close all;
clear all;

U_MIN = -1;
U_MAX = +1;
SAMPLES = 3000-1;

proc = SimulatedProcess(-1.272717, 0.332871, 0.028400, 0.019723, 0, 0, 0, 0);

timestamps = [0.05, 0.10, 0.15, 0.20, 0.30, 0.40, 0.50, 0.60, 0.70, 0.80, 0.90, 0.95];
u = createControlSequence(SAMPLES, timestamps, ...
    randi(100.*[U_MIN U_MAX], 1, length(timestamps) + 1)/100.0);

for i = 1:length(u)
    [proc, ~, ~, ~] = proc.compute(u(i));
end

plot(1:length(proc.get_y()), proc.get_y())
hold on
plot(1:length(proc.get_u()), proc.get_u(), 'k--')
title('Przebieg procesu wzbudzanego losowymi sygnałami')
ylabel('y')
xlabel('sample')
grid on

mkdir('zad_I_2');
saveas(gcf, [pwd '/zad_I_2/przebieg.png']);