close all;
clear all;

U_MIN = -1;
U_MAX = +1;
SAMPLES = 200;

STEADY_STATE_THRESHOLD = 1e-05;
REPEATS_MIN_NUM = 7;

u = linspace(U_MIN, U_MAX, SAMPLES);

proc = SimulatedProcess(-1.272717, 0.332871, 0.028400, 0.019723, 0, 0, 0, 0);

x1_prev = 0;    x2_prev = 0;    y_prev = 0;
x1_new = 0;     x2_new = 0;     y_new = 0;
repeats = 0;

for i = 1:length(u)
    % update process until values stop changing;
    % repeat at least REPEATS_MIN_NUM times - the process has response
    % delay
    while true
        x1_diff = abs(x1_prev - x1_new);
        x2_diff = abs(x2_prev - x2_new);
        y_diff = abs(y_prev - y_new);
        repeats = repeats + 1;
        if (x1_diff < STEADY_STATE_THRESHOLD && x2_diff < STEADY_STATE_THRESHOLD && y_diff < STEADY_STATE_THRESHOLD) && repeats >= REPEATS_MIN_NUM
            break;
        end
        x1_prev = x1_new;
        x2_prev = x2_new;
        y_prev = y_new;
        fprintf('u: %d | dx1: %4.5f, dx2: %4.5f, dy: %4.5f\n', u(i), x1_diff, x2_diff, y_diff);
        [proc, x1_new, x2_new, y_new] = proc.compute(u(i));
    end
    y(i) = y_new;
    repeats = 0;
end

plot(u, y)
title('Charakterystyka statyczna procesu')
subtitle('wyznaczona metodą symulacyjną')
ylabel('y')
xlabel('u')
grid on

mkdir('zad_I_1');
saveas(gcf, [pwd '/zad_I_1/charakterystyka_statyczna.png']);